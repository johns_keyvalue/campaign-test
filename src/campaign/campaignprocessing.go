package main

import (


	"campaign/app"
	//"time"
	//"fmt"

	"log"


	"github.com/go-gorp/gorp"
	"encoding/json"
	_ "github.com/go-sql-driver/mysql"
	"time"
	"strconv"
	"strings"
	"github.com/pborman/uuid"
	"campaign/scheduler"
	//"github.com/goadesign/goa"
	//"github.com/DavidHuie/gomigrate"

	//"github.com/goaddesign/goa/middleware"

	//"github.com/goadesign/goa"
	//"github.com/DavidHuie/gomigrate"
)



const CAMPAIGN_STATUS_CREATED int  = 1
const CAMPAIGN_STATUS_APPROVED int  = 2
const CAMPAIGN_STATUS_REJECTED int  = 4
const CAMPAIGN_STATUS_STARTED int  = 8
const CAMPAIGN_STATUS_EXECUTING int  = 16
const CAMPAIGN_STATUS_STOPPED int  = 32
const CAMPAIGN_STATUS_TERMINATED int  = 64
const BETWEEN string = "between"
const ONE_DAY_SEC int = 24 * 60 * 60
const INIT_POOL_NAME_SUFFIX = "_pool"
const SQL_VAR_DECL string = "set @csum := 0;"
const QUERY_PREFIX =  "select *, (@csum := @csum + demand_prob) as total_sum  from $poolName   where  available_date <= $currentDate "
const SUB_QUERIES_PREFIX = "and ( phone_number IN("
const SUB_QUERIES_SUFFIX = " ) "
const IF_NOT_EXISTS = " or (SELECT IF(COUNT(*) = 0,  true,false) AS result  FROM campaign_data WHERE phoneNumber = phone_number))"
const QUERY_SUFFIX = " and @csum < $totalScore"



type ProductInfoResponseCriteria struct {

	Variable string `json:"variable"`
	Comparison string `json:"comparison"`
	MinValue float32 `json:"min_value"`
	MaxValue float32 `json:"max_value"`
}


type ProductInfoResponseLocation struct {

	Province string `json:"province"`
	District string `json:"district"`
}


type ProductInfoResponseProdcut struct {

	Id string `json:"id"`
	ProductCode string `json:"product_code"`
	ProductType string `json:"product_type"`
	ClientCode string `json:"client_code"`
	DailyVolume int  `json:"daily_volume"`
	AvailableLocations []ProductInfoResponseLocation `json:"available_locations"`
	Criteria []ProductInfoResponseCriteria `json:"criteria"`
}

type ProductInfoResponseData struct {

	Product ProductInfoResponseProdcut `json:"product"`
}


type ProductInfoResponse struct {

	Verdict string `json:"verdict"`
	Message string `json:"message"`
	Data  ProductInfoResponseData  `json:"data"`

}

type CampaignJob struct {

	campaign *app.Campaign
	productInfo *ProductInfoResponse
	config *Config
	db *gorp.DbMap
	err error
	NewCycle bool
	ScheduledTime int64
	StartTime int64
	EndTime int64
	PollingInterval int64
	ExecutionFrequency int64
	ActiveStartPeriod int64
	ActiveEndPeriod int64
	ExecutionFrequencyUnit int
}

func (p *CampaignJob) SetNewCycle(val bool)  {

	p.NewCycle = val
}

func (p *CampaignJob) GetNewCycle() bool  {

	return p.NewCycle
}

func (p *CampaignJob) GetJobId() string {

	return *p.campaign.CampaignID
}

func (p *CampaignJob) GetStartTimeInSeconds() int64 {
	return p.StartTime
}
func (p *CampaignJob) GetEndTimeInSeconds() int64 {
	return p.EndTime
}

func (p *CampaignJob) GetActivePeriodStartInSeconds() int64 {
	return p.ActiveStartPeriod
}

func (p *CampaignJob) GetActivePeriodEndInSeconds() int64 {
	return p.ActiveEndPeriod
}

func (p *CampaignJob) GetExecutionFrequencyInSeconds() int64 {
	return p.ExecutionFrequency
}
func (p *CampaignJob) GetPollingIntervalInSeconds() int64 {
	return p.PollingInterval
}
func (p *CampaignJob) GetWeekOffDays() int {
	return 0 //TODO
}
/*func (p *CampaignJob)GetOffDays() []int64 {
       return 0;//TODO
}*/

func (p *CampaignJob) GetScheduledTime() int64 {

	return p.ScheduledTime
}

func (p *CampaignJob) SetScheduledTime(scheduledTime int64) {

	p.ScheduledTime = scheduledTime
}


func (p *CampaignJob)GetCopy() scheduler.Job {

	campCopy := new(CampaignJob)
	campCopy.NewCycle = false
	campCopy.ScheduledTime = 0

	*campCopy = *p
	return campCopy
}

func (p *CampaignJob)GetExecutionFrequencyUnit() int {
	return p.ExecutionFrequencyUnit
}

type InitPool struct {

	Phone_number   string
	Province string
	District string
	Credit_score float32
	Demand_score float32
	Demand_prob float32
	Income_level float32
	Update_date uint64
	Available_date uint64
	Total_sum float32
}
type InitPoolCollection []*InitPool

type CampaignData struct {

	CampaignDataId string
	PhoneNumber string
	CampaignId string
	MessageId string
	SentDate int64
	ReplyDate int64
	ForwardedDate int64
	FiId string
	Reply string
}


type CampaignMessage struct {

	CampaignId string
	MessageId string
	Percentage float32
	Content string
	NumToBeSent int
	totalSent  int
}

type CampaignMessageCollection []*CampaignMessage


func SelectCampaignMessages(db *gorp.DbMap,campId *string ) (CampaignMessageCollection,  error)  {

	campMessages := CampaignMessageCollection{}

	_, err := db.Select(&campMessages, "select cm.campaignId,cm.messageId,cm.percentage,mc.content from campaign_message cm, message_content mc where cm.campaignId='" +
		*campId + "' and cm.messageId = mc.messageId")
	return campMessages,err
}

func SelectCampaignMessagesTx(transaction *gorp.Transaction,campId *string ) (CampaignMessageCollection,  error)  {

	campMessages := CampaignMessageCollection{}

	_, err := transaction.Select(&campMessages, "select cm.campaignId,cm.messageId,cm.percentage,mc.content from campaign_message cm, message_content mc where cm.campaignId='" +
		*campId + "' and cm.messageId = mc.messageId")
	return campMessages,err
}

func SelectCampaign(db *gorp.DbMap, campId *string) (*app.Campaign,error) {

	var campaign  app.Campaign

	err := db.SelectOne(&campaign,"select * from campaign where campaignId = '" + *campId + "'")
	return &campaign,err
}

func SelectCampaignTx(transaction *gorp.Transaction, campId *string) (*app.Campaign,error) {

	var campaign  app.Campaign

	err := transaction.SelectOne(&campaign,"select * from campaign where campaignId = '" + *campId + "'")
	return &campaign,err
}

func UpdateCampaignTx(transaction *gorp.Transaction, campaign *app.Campaign) (*app.Campaign,error) {

	_,err := transaction.Update(campaign)
	return campaign,err
}

func SelectCampaigns(db *gorp.DbMap, status int) (app.CampaignCollection , error) {

	campaigns := app.CampaignCollection{}
	query := "select * from campaign where status & " + strconv.Itoa(status)
	_,err := db.Select(&campaigns,query) //STARTED OR APPROVED
	return campaigns,err
}

func InserCampaignExecutionContext(db *gorp.DbMap, campExeCtxt *app.CampaignExecutionContext) (*app.CampaignExecutionContext,error) {

	err := db.Insert(campExeCtxt)
	return campExeCtxt,err
}

func UpdateCampaignExecutionContext(db *gorp.DbMap, campExeCtxt *app.CampaignExecutionContext) (*app.CampaignExecutionContext,error) {

	_,err := db.Update(campExeCtxt)
	return campExeCtxt,err
}

func InsertCampaignExecutionContextTx(transaction *gorp.Transaction, campExeCtxt *app.CampaignExecutionContext) (*app.CampaignExecutionContext,error) {

	err := transaction.Insert(campExeCtxt)
	return campExeCtxt,err
}

func UpdateCampaignExecutionContextTx(transaction *gorp.Transaction, campExeCtxt *app.CampaignExecutionContext) (*app.CampaignExecutionContext,error) {

	_,err := transaction.Update(campExeCtxt)
	return campExeCtxt,err
}

func SelectCampaignExecutionContext(db *gorp.DbMap, id *string) (*app.CampaignExecutionContext,error) {

	var campaignExeCtxt  app.CampaignExecutionContext
	_,err := db.Get(&campaignExeCtxt,id)
	return &campaignExeCtxt,err
}


func InsertCampaignData(transaction *gorp.Transaction, campData *CampaignData) (*CampaignData,error) {

	err := transaction.Insert(campData)
	return campData,err
}



func getProductInfo(productId string)( *ProductInfoResponse,error){

	productInfo := []byte("{\"verdict\" : \"success\",    \"message\": \"get product successfully\",     \"data\":{\"product\": {       \"id\": \"1\",\"product_code\": \"CLWDB1\",\"product_type\": \"loan\", \"client_code\": \"home_credit\",      \"daily_volume\": 1000,\"available_locations\": [      {\"province\": \"province1\",  \"district\": \"district1\"},{\"province\": \"TP HCM\",\"district\": \"Q2\"}],\"criteria\": [{\"variable\": \"credit_score\",\"comparison\": \"between\",\"min_value\": 400,\"max_value\": 500}]}}}")
	var  productInfoResponse ProductInfoResponse
	err := json.Unmarshal( productInfo, &productInfoResponse)
	return &productInfoResponse,err
}


func addCriteriaConditions(query string, criterias []ProductInfoResponseCriteria) string {

	if nil != criterias && len(criterias ) > 0 {
		query +=  " and ("
		criteria  := criterias[0]
		if strings.ToLower(criteria.Comparison) ==  BETWEEN {
			query +=  " ( " + criteria.Variable +  " " + BETWEEN + " " + strconv.FormatFloat(float64(criteria.MinValue), 'f', -1, 32) + " AND " + strconv.FormatFloat(float64(criteria.MaxValue), 'f', -1, 32)  + " )"
		}

		for i := 1; i < len(criterias); i++ {
			criteria = criterias[i]
			if strings.ToLower(criteria.Comparison) ==  BETWEEN { //Only "between support"
				query += " and  ( " + criteria.Variable + " " + BETWEEN + " " + strconv.FormatFloat(float64(criteria.MinValue), 'f', -1, 32) + " AND " + strconv.FormatFloat(float64(criteria.MaxValue), 'f', -1, 32) + " )"
			}
		}
		query +=  ") "
	}
	return query
}

func addLocationsConditions(query string, locations []ProductInfoResponseLocation) string {

	if nil != locations && len(locations ) > 0 {
		query +=  " and ("
		loc := locations[0]
		if loc.Province != "" {
			query += "(province = '" + loc.Province + "' "
			if loc.District != "" {
				query += ")"
			} else {
				query += " and distric = '" + loc.District + "' ) "
			}
		}

		for i := 1; i < len(locations); i++ {

			loc = locations[i]
			if loc.Province != "" {
				query += " or ( province = '" + loc.Province + "' "
				if loc.District != "" {
					query += ")"
				} else {
					query += " and distric = '" + loc.District + "' ) "
				}
			}
		}
		query += ")"
	}
	return query
}


func prepareQuery(
	now int64,
	totalScore int,
	campaignCtxt *CampaignJob) string {

	poolName := campaignCtxt.productInfo.Data.Product.ProductType + INIT_POOL_NAME_SUFFIX
	locations := campaignCtxt.productInfo.Data.Product.AvailableLocations
	criteria :=  campaignCtxt.productInfo.Data.Product.Criteria

	strReplacer := strings.NewReplacer( "$poolName" , poolName,
		"$currentDate", strconv.Itoa(int(now)),
		"$fi", campaignCtxt.productInfo.Data.Product.ClientCode,
		"$totalScore",strconv.Itoa(totalScore))

	// Replace all pairs.

	query := QUERY_PREFIX

	selectQueries := campaignCtxt.config.SelectionConfig.SelectQueries;
	if nil != selectQueries && len(selectQueries) > 0  {

		query += SUB_QUERIES_PREFIX
		query +=  selectQueries[0]

		for i := 1; i < len(selectQueries); i++ {

			query += " union "

			query += selectQueries[i]
		}
		query += SUB_QUERIES_SUFFIX

		query += IF_NOT_EXISTS
	}

	query = addCriteriaConditions(query,criteria)
	query = addLocationsConditions(query,locations)
	query +=   QUERY_SUFFIX

	query = strReplacer.Replace(query)

	log.Println("Query :" ,query)

	return query
}


func checkErr(err error, msg string) {
	if err != nil {
		log.Printf(msg, err)
	}
}


func getCurrentTimeUnix() int {

	loc, _ := time.LoadLocation("UTC")
	return  int(time.Now().In(loc).Unix())
}



func getCurrentQueueSize(productId string) (int,error) {

	//TODO
	return 400,nil
}

func getTotalLeadProducedInthisCycle(productId string, since int) (int,error) {

	//TODO
	return 300,nil
}

func calculateNumMessages( totalNumMessages int ,campaignMessages CampaignMessageCollection) {

	var count int = 0

	for _,campaignMessage := range campaignMessages {

		campaignMessage.totalSent = 0
		perc := int(float32(totalNumMessages) * campaignMessage.Percentage/ 100)
		campaignMessage.NumToBeSent = perc
		count += perc
	}
	if count != totalNumMessages {

		campaignMessages[0].NumToBeSent += totalNumMessages - count
	}
}

func createCampaignData(campaignCtxt *CampaignJob,
	messageId string,
	phoneNum string,
	currTime int,
	transaction *gorp.Transaction ) (*CampaignData,error) {

	campaignData  := CampaignData{}
	campaignData.CampaignDataId = uuid.New()
	campaignData.CampaignId = *campaignCtxt.campaign.CampaignID
	campaignData.MessageId = messageId
	campaignData.FiId = campaignCtxt.productInfo.Data.Product.ClientCode; //?
	campaignData.SentDate = int64(currTime)
	campaignData.PhoneNumber = phoneNum
	return InsertCampaignData(transaction,&campaignData)

}

func updateInitPool(campaignCtxt *CampaignJob,
	phoneNum string,
	currTime int,
	transaction *gorp.Transaction) error {

	newTime := campaignCtxt.config.SelectionConfig.PoolAvailability * ONE_DAY_SEC + currTime
	_,err := transaction.Exec("UPDATE " + campaignCtxt.productInfo.Data.Product.ProductType + INIT_POOL_NAME_SUFFIX + " SET available_date = " + strconv.Itoa(newTime) + " WHERE phone_number =  '" + phoneNum + "'");
	return err
}

func doCampaign(campaignCtxt *CampaignJob,
	query string,
	size int ,
	executionCtxt *app.CampaignExecutionContext,
	campaignMessages CampaignMessageCollection) error {

	log.Println("Executing campaign Batch with size " , strconv.Itoa(size))
	currTime := getCurrentTimeUnix()
	calculateNumMessages(size,campaignMessages)

	transaction, err := campaignCtxt.db.Begin()
	if nil != err {
		return err
	}

	transaction.Exec(SQL_VAR_DECL)
	transaction.Exec("update campaign set status=? where campaignId=?",CAMPAIGN_STATUS_STARTED,*campaignCtxt.campaign.CampaignID)
	resHolder := InitPoolCollection{}
	_, err = transaction.Select(&resHolder,query)
	if  nil != err {

		transaction.Rollback()
		return err
	}

	var messageIndex int = 0

	for _,initPool := range resHolder{

		_,err = createCampaignData(campaignCtxt,campaignMessages[messageIndex].MessageId,initPool.Phone_number,currTime,transaction)
		if nil != err {

			transaction.Rollback()
			return err
		}

		err = updateInitPool(campaignCtxt,initPool.Phone_number,currTime,transaction)
		if nil != err {

			transaction.Rollback()
			return err
		}
		campaignMessages[messageIndex].totalSent++
		if campaignMessages[messageIndex].NumToBeSent == campaignMessages[messageIndex].totalSent {

			messageIndex++
		}
	}

	*executionCtxt.NumMessagesSent += len(resHolder)
	currTime = getCurrentTimeUnix()
	executionCtxt.EndTime = &currTime

	_,err = UpdateCampaignExecutionContextTx(transaction,executionCtxt)
	if nil != err {
		return err
	}
	err = transaction.Commit()
	return err
}



func IsCampaignExecutable(campaign *app.Campaign) bool {

	return (0 != *campaign.Status & CAMPAIGN_STATUS_APPROVED  || 0 != *campaign.Status & CAMPAIGN_STATUS_STARTED)
}


func createExecutionCtxtAndUpdateCampaign(campaignCtxt *CampaignJob, currTime int) (*app.CampaignExecutionContext,CampaignMessageCollection, error) {


	executionCtxt := app.CampaignExecutionContext{}
	tx,err := campaignCtxt.db.Begin()

	if nil != err {

		return nil,nil,err
	}

	campaign, err := SelectCampaignTx(tx, campaignCtxt.campaign.CampaignID)
	if nil != err {
		return nil,nil,err
	}

	campaignMessages, err := SelectCampaignMessagesTx(tx,campaign.CampaignID)

	if nil != err {

		return nil,nil,err
	}
	if IsCampaignExecutable(campaign)  {

		//*campaignCtxt.campaign.Status |=  CAMPAIGN_STATUS_EXECUTING
		_,err = UpdateCampaignTx(tx,campaignCtxt.campaign)
		if nil != err {
			tx.Rollback()
			return nil,nil,err
		}

		id := uuid.New()
		temp := 0
		executionCtxt.NumMessagesSent = &temp
		executionCtxt.ExecutionID = &id
		executionCtxt.StartTime = &currTime
		executionCtxt.EndTime = &currTime

		executionCtxt.CampaignID = campaignCtxt.campaign.CampaignID

		len := 0
		executionCtxt.NumMessagesSent = &len

		_,err = InsertCampaignExecutionContextTx(tx, &executionCtxt)
		if nil != err {

			tx.Rollback()
			return nil,nil,err
		}
		tx.Commit()
	}
	return &executionCtxt,campaignMessages,nil
}

func executeCampaign(  sched *scheduler.Scheduler,
	campaignCtxt *CampaignJob,
	targetVolume int) {

	loc, _ := time.LoadLocation("UTC")
	nowTime := time.Now().In(loc)
	now := nowTime.Unix()

	if nil == campaignCtxt.err {

		if targetVolume > 0 {

			executionCtxt, campaignMessages, err := createExecutionCtxtAndUpdateCampaign(campaignCtxt, int(now))

			if nil == err  {

				batch := targetVolume / campaignCtxt.config.SelectionConfig.TransactionSize
				if batch > 0 {
					query := prepareQuery(now, campaignCtxt.config.SelectionConfig.TransactionSize, campaignCtxt)

					for i := 0; i < batch; i++ {

						err = doCampaign(campaignCtxt, query, campaignCtxt.config.SelectionConfig.TransactionSize, executionCtxt, campaignMessages);
						if nil != err {

							checkErr(err, "Unable to execute Campaign.....")
							msg := err.Error()
							executionCtxt.Error = &msg
							UpdateCampaignExecutionContext(campaignCtxt.db, executionCtxt)
							break
						}
					}
				}

				if nil == err {

					remaining := targetVolume % campaignCtxt.config.SelectionConfig.TransactionSize

					if 0 != remaining {

						query := prepareQuery(now, remaining, campaignCtxt);
						err = doCampaign(campaignCtxt, query, remaining, executionCtxt, campaignMessages)

						if nil != err {

							checkErr(err, "Unable to execute Campaign....")
							msg := err.Error()
							executionCtxt.Error = &msg
							UpdateCampaignExecutionContext(campaignCtxt.db, executionCtxt)

						}
					}
				} else {

					checkErr(err, "Unable to execute Campaign...")
					campaignCtxt.err = err
				}
			} else {
				checkErr(err, "Unable to execute Campaign..")
				campaignCtxt.err = err
			}

		}
	}

	if nil != campaignCtxt.err {
		sched.Remove(*campaignCtxt.campaign.CampaignID)
		scheduleCampaign(sched,campaignCtxt.campaign,campaignCtxt.config,campaignCtxt.db)
	}
}

func processCampaign(
	scheduler *scheduler.Scheduler,
	campaignCtxt *CampaignJob) {
	var target int = 0
	if nil == campaignCtxt.err {
		if campaignCtxt.NewCycle {
			scheduledTime  := int(campaignCtxt.ScheduledTime)
			campaignCtxt.campaign.CurrentExecutionCycleStartTime = &scheduledTime
			campaignCtxt.campaign.CurrentTargetVolume = &target
			currQueueSize, err := getCurrentQueueSize(campaignCtxt.productInfo.Data.Product.Id)

			if nil == err {
				if currQueueSize < campaignCtxt.productInfo.Data.Product.DailyVolume {
					target = campaignCtxt.productInfo.Data.Product.DailyVolume - currQueueSize // This is just an approximation as new lead can be generated at any time
					campaignCtxt.campaign.CurrentTargetVolume = &target //invoke synchronously
				}
			} else {
				campaignCtxt.err = err
			}
		} else  {

			total,err := getTotalLeadProducedInthisCycle(campaignCtxt.productInfo.Data.Product.Id, *campaignCtxt.campaign.CurrentExecutionCycleStartTime)
			if nil == err {
				if total < *campaignCtxt.campaign.CurrentTargetVolume {
					target = *campaignCtxt.campaign.CurrentTargetVolume - total // approximation
				}
			} else {
				campaignCtxt.err = err
			}
		}
	}
	executeCampaign(scheduler,campaignCtxt, target)
}

func startCampaignProcessing(scheduler *scheduler.Scheduler ) {

	for true {
		campaignJob := <-scheduler.JobChannel
		processCampaign(scheduler,campaignJob.(*CampaignJob) )

	}
}

func scheduleCampaign( sch  *scheduler.Scheduler,campaign *app.Campaign,config* Config,	db *gorp.DbMap) error {
	if int64(*campaign.EndDate) <= time.Now().Unix() {
		return nil
	}
	productInfo,err := getProductInfo(*campaign.ProductID)
	if nil != err {
		checkErr(err,"Unable to fetch product Info")
	}

	pollingInterval := int64(*campaign.PollingInterval) * scheduler.ONE_MNT_SECONDS
	executionFreq   := int64(*campaign.ExecutionFrequency) * scheduler.ONE_DAY_SECONDS
	activeStart := int64(*campaign.ActiveStartHour) * scheduler.ONE_HOUR_SECONDS + int64(*campaign.ActiveStartMinute) * scheduler.ONE_MNT_SECONDS
	activeEnd :=  activeStart + int64(*campaign.ActiveHoursInMinutes) *  scheduler.ONE_MNT_SECONDS

	campJob := CampaignJob{
		campaign:campaign,
		config:config,
		db:db,
		err:err,
		productInfo:productInfo,
		NewCycle: false,
		ScheduledTime:0,
		StartTime: int64(*campaign.StartDate),
		EndTime : int64(*campaign.EndDate),
		PollingInterval : pollingInterval,
		ExecutionFrequency : executionFreq,
		ActiveStartPeriod  : activeStart,
		ActiveEndPeriod :activeEnd,
		ExecutionFrequencyUnit:scheduler.EXECUTION_FREQUENCY_UNIT_DAYS}
	return  sch.Schedule(&campJob)
}

func scheduleCampaigns(scheduler *scheduler.Scheduler,config* Config,db *gorp.DbMap ) error {
	campaigns,err := SelectCampaigns(db, CAMPAIGN_STATUS_APPROVED | CAMPAIGN_STATUS_STARTED)
	var init int = 0
	if nil == err {
		for _, campaign := range campaigns {

			if nil == campaign.LastExecutionTime {
				campaign.LastExecutionTime = &init
			}
			if nil == campaign.CurrentExecutionCycleStartTime {
				campaign.CurrentExecutionCycleStartTime = &init
			}
			err = scheduleCampaign(scheduler,campaign,config,db)
			if nil != err {
				break
			}

		}
	}
	return err
}

func  StartCampaign(config* Config,db *gorp.DbMap,scheduler *scheduler.Scheduler) error{

	err := scheduleCampaigns(scheduler,config,db)
	
	if nil == err {
		go startCampaignProcessing(scheduler)
	}
	return err
}
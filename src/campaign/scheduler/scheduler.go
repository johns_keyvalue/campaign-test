package scheduler

import (

	"time"
	"log"

	"sync"
	"errors"
)

type NowFuncType func() time.Time

var nowFunc NowFuncType = func() time.Time {
	return time.Now()
}


func SetNowFunction( newNow NowFuncType) NowFuncType {//For testing
	old := nowFunc
	nowFunc = newNow;
	return old;
}



type Job interface {

	SetNewCycle(val bool)
	GetNewCycle() bool
	GetScheduledTime() int64
	SetScheduledTime(scheduledTime int64)
	GetJobId() string
	GetStartTimeInSeconds() int64
	GetEndTimeInSeconds() int64
	GetActivePeriodStartInSeconds() int64
	GetActivePeriodEndInSeconds() int64
	GetExecutionFrequencyInSeconds() int64
	GetPollingIntervalInSeconds() int64
	GetWeekOffDays() int
	GetExecutionFrequencyUnit() int
	//GetOffDays() []int
	GetCopy() Job
}

const (
	EXECUTION_FREQUENCY_UNIT_DAYS int = 0
	EXECUTION_FREQUENCY_UNIT_HOURS int = 1
	EXECUTION_FREQUENCY_UNIT_MINUTES int = 2
)
const ONE_MNT_SECONDS int64 =   60
const ONE_HOUR_SECONDS int64 =   ONE_MNT_SECONDS * 60
const ONE_DAY_SECONDS int64 = 24 * ONE_HOUR_SECONDS

const DEFAULT_BUFFER_SIZE int = 100


type Scheduler struct {

	timers  map[string]*time.Timer
	JobChannel chan Job
	mutex  *sync.Mutex
}


func New() *Scheduler {

	scheduler := new(Scheduler)
	scheduler.timers = make(map[string]*time.Timer)
	scheduler.JobChannel = make(chan Job,DEFAULT_BUFFER_SIZE);
	scheduler.mutex = new(sync.Mutex)
	return scheduler
}





func getActiveTime(unit int,
	timeObject* time.Time,
	loc *time.Location,
	activeStartTime int64) int64 {

	if (unit == EXECUTION_FREQUENCY_UNIT_DAYS) {

		hours := activeStartTime/ONE_HOUR_SECONDS
		mnts :=  (activeStartTime % ONE_HOUR_SECONDS)/ONE_MNT_SECONDS;

		return time.Date(timeObject.Year(), timeObject.Month(), timeObject.Day(), int(hours), int(mnts), 0, 0, loc).Unix();
	} else if(unit == EXECUTION_FREQUENCY_UNIT_HOURS) {

		mnts := activeStartTime/ONE_MNT_SECONDS
		secnds :=  activeStartTime % ONE_MNT_SECONDS;

		return time.Date(timeObject.Year(), timeObject.Month(), timeObject.Day(), timeObject.Hour(), int(mnts), int(secnds), 0, loc).Unix();
	} else {

		secnds := activeStartTime%ONE_MNT_SECONDS
		return time.Date(timeObject.Year(), timeObject.Month(), timeObject.Day(), timeObject.Hour(), timeObject.Minute(), int(secnds), 0, loc).Unix();
	}
}


func incrementToNextCycle(unit int, timeObject* time.Time,loc *time.Location, activeStart int64) int64 {

	if (unit == EXECUTION_FREQUENCY_UNIT_DAYS) {

		hours := activeStart / ONE_HOUR_SECONDS
		mnts :=  activeStart % ONE_HOUR_SECONDS / ONE_MNT_SECONDS;

		return time.Date(timeObject.Year(), timeObject.Month(), timeObject.Day() + 1, int(hours), int(mnts), 0, 0, loc).Unix();
	} else if(unit == EXECUTION_FREQUENCY_UNIT_HOURS) {

		mnts := activeStart/ONE_MNT_SECONDS
		secnds :=  activeStart % ONE_MNT_SECONDS;

		return time.Date(timeObject.Year(), timeObject.Month(), timeObject.Day(), timeObject.Hour() + 1, int(mnts), int(secnds), 0, loc).Unix();
	} else {

		secnds := activeStart %ONE_MNT_SECONDS
		return time.Date(timeObject.Year(), timeObject.Month(), timeObject.Day(), timeObject.Hour(), timeObject.Minute() + 1, int(secnds), 0, loc).Unix();
	}
}


func calculateDuration(
	unit int,
	pollingInterval int64,
	startTime int64,
	endTime int64,
	activeStart int64,
	activeEnd int64,
	now *time.Time) (int64,int64,int64,int64) {

	nowTime := now.Unix()

	loc, _ := time.LoadLocation("UTC")

	startTimeObj := time.Unix(startTime,0);


	if(nowTime > startTime) {

		activeStartTime := getActiveTime(unit,now,loc,activeStart);

		activeEndTime := getActiveTime(unit,now,loc,activeEnd);

		var actualStartTime int64 =0 ;
		if(activeStartTime >= nowTime) {

			actualStartTime = activeStartTime
		} else {
			actualStartTime = nowTime + pollingInterval - (nowTime - activeStartTime) % pollingInterval
		}

		if(actualStartTime >= endTime) {
			return -1,-1,-1,-1
		}

		if(actualStartTime >= activeEndTime) {

			actualStartTime = incrementToNextCycle(unit,now,loc,activeStart) ;
		}

		if(actualStartTime >= endTime) {
			return -1,-1,-1,-1
		}
		return  actualStartTime - nowTime,actualStartTime, activeStartTime, activeEndTime
	} else {

		activeStartTime := getActiveTime(unit,&startTimeObj,loc,activeStart) ;
		if(activeStartTime >= startTime) {
			activeStartTime = startTime;
		} else {
			activeStartTime = startTime + pollingInterval - (startTime - activeStartTime) % pollingInterval
		}

		activeEndTime := getActiveTime(unit,&startTimeObj,loc,activeEnd)

		return  activeStartTime - nowTime ,activeStartTime,activeStartTime,activeEndTime;
	}

}



func getDuration(job Job) (int64 ,int64,int64,int64) {

	loc, _ := time.LoadLocation("UTC")
	now := nowFunc().In(loc)

	return calculateDuration(
		job.GetExecutionFrequencyUnit(),
		job.GetPollingIntervalInSeconds(),
		job.GetStartTimeInSeconds(),
		job.GetEndTimeInSeconds(),
		job.GetActivePeriodStartInSeconds(),

		job.GetActivePeriodEndInSeconds(),
		&now)



}

func isNewCycle( unit int, executionFrequency int64, lastTime int64, scheduledTime int64) bool {


	return lastTime/ executionFrequency  != scheduledTime  /executionFrequency

}


func scheduleJob(timer *time.Timer, scheduler *Scheduler,job Job) {

	<-timer.C


	scheduler.JobChannel<-job;



	duration,scheduledTime,effectiveStartTime,effectiveEndTime := getDuration(job);

	if(-1 != duration ) {


		newCycle :=  isNewCycle(job.GetExecutionFrequencyUnit(),job.GetExecutionFrequencyInSeconds(),job.GetScheduledTime(),scheduledTime)


		jobCopy := job.GetCopy()
		jobCopy.SetScheduledTime(scheduledTime)
		jobCopy.SetNewCycle(newCycle)

		printTimes(jobCopy.GetStartTimeInSeconds(), jobCopy.GetEndTimeInSeconds(), jobCopy.GetNewCycle(), job.GetScheduledTime(), effectiveStartTime, effectiveEndTime, jobCopy.GetScheduledTime())
		timer.Reset(time.Duration(duration) * time.Second)
		go scheduleJob(timer, scheduler, jobCopy)
	}
}


func (scheduler *Scheduler) validate(job Job) error {

	if(nil == job) {
		return  errors.New("No job specified.")
	}
	_,ok := scheduler.timers[job.GetJobId()]
	if(ok) {
		return  errors.New("Job with id '" +  job.GetJobId() + "' exists.")
	}

	if(job.GetPollingIntervalInSeconds() > job.GetActivePeriodEndInSeconds() - job.GetActivePeriodStartInSeconds()) {

		return  errors.New("Invalid polling interval.")
	}
	unit := job.GetExecutionFrequencyUnit();
	if(unit == EXECUTION_FREQUENCY_UNIT_DAYS) {

		if(job.GetActivePeriodStartInSeconds()< 0 || job.GetActivePeriodStartInSeconds() >= ONE_DAY_SECONDS) {

			return  errors.New("Invalid period start")
		}

		if(job.GetActivePeriodEndInSeconds() <= 0 || job.GetActivePeriodEndInSeconds() > ONE_DAY_SECONDS) {
			return  errors.New("Invalid period end")
		}

		if(job.GetExecutionFrequencyInSeconds() % ONE_DAY_SECONDS != 0) {
			return  errors.New("Invalid execution frequency.")
		}



	} else if(unit == EXECUTION_FREQUENCY_UNIT_HOURS) {

		if(job.GetActivePeriodStartInSeconds()< 0 || job.GetActivePeriodStartInSeconds() >= ONE_HOUR_SECONDS) {

			return  errors.New("Invalid period start")
		}

		if(job.GetActivePeriodEndInSeconds() <= 0 || job.GetActivePeriodEndInSeconds() > ONE_HOUR_SECONDS) {
			return  errors.New("Invalid period end")
		}

		if(job.GetExecutionFrequencyInSeconds() % ONE_HOUR_SECONDS != 0) {
			return  errors.New("Invalid execution frequency.")
		}
	} else if(unit == EXECUTION_FREQUENCY_UNIT_MINUTES) {

		if(job.GetActivePeriodStartInSeconds()< 0 || job.GetActivePeriodStartInSeconds() >= ONE_MNT_SECONDS) {

			return  errors.New("Invalid period start")
		}

		if(job.GetActivePeriodEndInSeconds() <= 0 || job.GetActivePeriodEndInSeconds() > ONE_MNT_SECONDS) {
			return  errors.New("Invalid period end")
		}

		if(job.GetExecutionFrequencyInSeconds() % ONE_MNT_SECONDS != 0) {
			return  errors.New("Invalid execution frequency.")
		}

	} else {

		return  errors.New("Invalid execution frequency unit.")
	}
	if(job.GetActivePeriodStartInSeconds() >= job.GetActivePeriodEndInSeconds()) {

		return  errors.New("Invalid active period.")
	}

	if(job.GetStartTimeInSeconds() >= job.GetEndTimeInSeconds()) {

		return  errors.New("Invalid start time and end time.")
	}

	if(job.GetEndTimeInSeconds() <= nowFunc().Unix()) {

		return  errors.New("Expired job.")

	}

	return nil;
}

func (scheduler *Scheduler) Schedule(job Job) error {

	scheduler.mutex.Lock()
	defer scheduler.mutex.Unlock()
	
	err := scheduler.validate(job)
	if(nil != err) {
		return err;
	}
	duration,scheduledTime,effectiveStartTime,effectiveEndTime := getDuration(job);

	if(-1 != duration) {


		newCycle :=  true;

		printTimes(job.GetStartTimeInSeconds(), job.GetEndTimeInSeconds(), newCycle, job.GetScheduledTime(), effectiveStartTime, effectiveEndTime, scheduledTime)

		jobCopy := job.GetCopy()
		jobCopy.SetScheduledTime(scheduledTime)
		jobCopy.SetNewCycle(newCycle)

		timer := time.NewTimer(time.Duration(duration) * time.Second)
		scheduler.timers[job.GetJobId()] = timer;

		go scheduleJob(timer, scheduler, jobCopy)
	}

	
	return nil;
}






func (scheduler *Scheduler)Remove(jobId string) {

	scheduler.mutex.Lock()
	defer scheduler.mutex.Unlock()
	
	val,ok := scheduler.timers[jobId]
	if(ok ) {
		val.Stop()
	}
	delete(scheduler.timers,jobId)
	
}


func (scheduler *Scheduler) clearInternal() {
	for  _,timer := range scheduler.timers {
		timer.Stop()
	}

}

func (scheduler *Scheduler) Clear() {

	scheduler.mutex.Lock()
	defer scheduler.mutex.Unlock()
	
	scheduler.clearInternal();
	scheduler.timers = make(map[string]*time.Timer)
	
}


func (scheduler *Scheduler) Close() {

	scheduler.mutex.Lock()
	defer scheduler.mutex.Unlock()
	scheduler.clearInternal();
	//close(scheduler.JobChannel)
	
}


func printTimes(campStartTime int64,
	campEndTime int64,
	newCycle bool,
	lastExecutionTime int64,
	activeHoursStart int64,
	activeHoursEnd int64,
	scheduledTime int64) { //For debugging

	loc, _ := time.LoadLocation("UTC")
	now := nowFunc().In(loc)

	log.Println("****************************************************************************")

	log.Println("                 Now :" , now)
	if(0 != scheduledTime) {
		log.Println("      Scheduled Time :", time.Unix(int64(scheduledTime), 0).In(loc))
	}

	if(0 != activeHoursStart) {
		log.Println("EffectiveStartTime   :", time.Unix(int64(activeHoursStart), 0).In(loc))
	}

	if(0 != activeHoursEnd) {

		log.Println(" EffectiveEndTime    :", time.Unix(int64(activeHoursEnd), 0).In(loc))
	}
	if(newCycle) {
		log.Println("   New Cycle         :", newCycle)
	}

	if(0 != lastExecutionTime) {
		log.Println(" Last Execution Time :", time.Unix(int64(lastExecutionTime), 0).In(loc))
	}

	if(0 != campStartTime) {
		log.Println(" Job Start Time :", time.Unix(int64(campStartTime), 0).In(loc))
	}

	if(0  != campEndTime) {
		log.Println("   Job End Time :", time.Unix(int64(campEndTime), 0).In(loc))
	}



}




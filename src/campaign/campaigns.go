package main

import (
	"bytes"
	"campaign/app"
	"fmt"
	"github.com/satori/go.uuid"
	"github.com/goadesign/goa"
	"strings"
	"database/sql"
	"encoding/json"
	"reflect"
	"campaign/scheduler"
	"github.com/go-gorp/gorp"
	"log"
)

// CampaignsController implements the campaigns resource.
type CampaignsController struct {
	*goa.Controller
	*sql.DB
	*scheduler.Scheduler
	*Config
	*gorp.DbMap
}

// NewCampaignsController creates a campaigns controller.
func NewCampaignsController(service *goa.Service, db *sql.DB, scheduler *scheduler.Scheduler,config *Config,dbMap *gorp.DbMap) *CampaignsController {
	return &CampaignsController{Controller: service.NewController("CampaignsController"), DB: db,Scheduler: scheduler,Config:config,DbMap:dbMap}
}


// Create runs the create action.
func (c *CampaignsController) Create(ctx *app.CreateCampaignsContext) error {
	// CampaignsController_Create: start_implement
	type campaign struct {
		CampaignId string `form:"campaignId" json:"campaignId" xml:"campaignId"`
		app.CampaignPayload
		app.Campaign
	}
	if valid,reason :=campaignCreationValidation(ctx); !valid{

		return ctx.BadRequest([]byte(reason))
	}
	id := uuid.NewV4()
	trans, _ := c.DB.Begin()
	_, err := trans.Exec("insert INTO campaign(campaignId,productId,status,startDate,"+
		"endDate,activeStartHour,activeStartMinute,activeHoursInMinutes,pollingInterval,executionFrequency) VALUES (?,?,1,?,?,?,?,?,?,?)", id,
		ctx.Payload.ProductID,
		ctx.Payload.StartDate,
		ctx.Payload.EndDate,
		ctx.Payload.ActiveStartHour,
		ctx.Payload.ActiveStartMinute,
		ctx.Payload.ActiveHoursInMinutes,
		ctx.Payload.PollingInterval,
		ctx.Payload.ExecutionFrequency)

	if err != nil {
		trans.Rollback()
		return ctx.InternalServerError([]byte(err.Error()))
	}

	for _, message := range ctx.Payload.Messages {
		_, err = trans.Exec("INSERT INTO campaign_message(campaignId,messageId,percentage) VALUES(?,?,?)", id, message.MessageID, message.Percentage)
		if err != nil {
			trans.Rollback()
			return ctx.InternalServerError([]byte(err.Error()))

		}
	}
	err = trans.Commit()
	if err != nil {
		trans.Rollback()
		return ctx.InternalServerError([]byte(err.Error()))
	}
	var campaignInstance  campaign
	campaignInstance.CampaignPayload=*ctx.Payload
	campaignInstance.CampaignId =id.String()

	b,_:=json.Marshal(campaignInstance)
	ctx.ResponseData.Write(b)
	// CampaignsController_Create: end_implement
	return nil // input clone with adding uuid
}

// Delete runs the delete action.
func (c *CampaignsController) Delete(ctx *app.DeleteCampaignsContext) error {
	// CampaignsController_Delete: start_implement
	trans, _ := c.DB.Begin()
	row,err := trans.Exec("DELETE from campaign_message where campaignId=?",ctx.CampaignID)
	if err != nil {
		trans.Rollback()
		return ctx.NotFound()
	}
	if err != nil {
		fmt.Println(err)
		trans.Rollback()
		return ctx.Err()
	}
	row,err = trans.Exec("DELETE FROM campaign WHERE campaignId =?",ctx.CampaignID)
	if count,_ := row.RowsAffected(); count!=1 {
		trans.Rollback()
		return ctx.NotFound()
	}
	if err != nil {
		trans.Rollback()
		return ctx.Err()
	}
	err =trans.Commit()
	if err != nil {
		trans.Rollback()
		fmt.Println(err)
		return ctx.NotFound()
	}
	return ctx.Deleted()
	// CampaignsController_Delete: end_implement
}

// Get runs the get action.
func (c *CampaignsController) Get(ctx *app.GetCampaignsContext) error {
	// CampaignsController_Get: start_implement

	stmt, _ := c.DB.Prepare("select campaignId,productId,status,startDate," +
		"endDate,activeStartHour,activeStartMinute,activeHoursInMinutes,pollingInterval,executionFrequency from campaign where campaignId =?")
	row, _ := stmt.Query(ctx.CampaignID)
	defer row.Close()
	//checkErr(err)
	if !row.Next() {
		return ctx.NotFound()
	}
	var campaign app.CampaignDetailed
	_ = row.Scan(&campaign.CampaignID,
		&campaign.ProductID,
		&campaign.Status,
		&campaign.StartDate,
		&campaign.EndDate,
		&campaign.ActiveStartHour,
		&campaign.ActiveStartMinute,
		&campaign.ActiveHoursInMinutes,
		&campaign.PollingInterval,
		&campaign.ExecutionFrequency)

	// CampaignsController_Get: end_implement
	return ctx.OKDetailed(&campaign)
}

// GetAll runs the getAll action.
func (c *CampaignsController) GetAll(ctx *app.GetAllCampaignsContext) error {
	// CampaignsController_GetAll: start_implement

	var condition string
	var offset, numOfResults int
	offset,numOfResults = getPaginationParams(ctx)
	condition = getCondition(ctx)
	stmt, _ := c.DB.Prepare("select campaignId,productId,status,startDate," +
		"endDate,activeStartHour,activeStartMinute,activeHoursInMinutes,pollingInterval,executionFrequency from campaign" + condition + " limit ?,?")
	row, _ := stmt.Query(offset, numOfResults)
	defer row.Close()
	var campaigns app.CampaignCollection
	var campaign app.Campaign
	for row.Next() {
		row.Scan(&campaign.CampaignID,
			&campaign.ProductID,
			&campaign.Status,
			&campaign.StartDate,
			&campaign.EndDate,
			&campaign.ActiveStartHour,
			&campaign.ActiveStartMinute,
			&campaign.ActiveHoursInMinutes,
			&campaign.PollingInterval,
			&campaign.ExecutionFrequency)
		newCampaign := campaign
		fmt.Println(&newCampaign)
		campaigns = append(campaigns, &newCampaign)
	}
	fmt.Println(campaigns)

	// CampaignsController_GetAll: end_implement
	return ctx.OK(campaigns)
}

// GetAllCampaignExecution runs the getAllCampaignExecution action.
func (c *CampaignsController) GetAllCampaignExecution(ctx *app.GetAllCampaignExecutionCampaignsContext) error {
	// CampaignsController_GetAllCampaignExecution: start_implement

	res,_ :=SelectCampaignExecutionContext(c.DbMap,&ctx.CampaignID)
	log.Println(res.CampaignID)
	// CampaignsController_GetAllCampaignExecution: end_implement
	return ctx.OK(res)
}

// GetCampaignExecution runs the getCampaignExecution action.
func (c *CampaignsController) GetCampaignExecution(ctx *app.GetCampaignExecutionCampaignsContext) error {
	// CampaignsController_GetCampaignExecution: start_implement

	// Put your logic here

	// CampaignsController_GetCampaignExecution: end_implement
	res := &app.CampaignExecutionContext{}
	return ctx.OK(res)
}

// Update runs the update action.
func (c *CampaignsController) Update(ctx *app.UpdateCampaignsContext) error {
	// CampaignsController_Update: start_implement
	stmt, _ := c.DB.Prepare("SELECT * FROM campaign WHERE campaignId =?")
	row, err := stmt.Query(ctx.CampaignID)
	if err != nil {
		fmt.Println(err)
	}
	defer row.Close()
	if !row.Next() {
		return ctx.NotFound()
	}
	var messagePercentage= make(map[string]float64)
	var totalPercentage= 0.0
	var columns= getTableColumns(c.DB, "campaign", "campaign")
	var columnsToUpdate= getColumnsToUpdate(ctx, columns)
	var buffer bytes.Buffer
	// validate the start date and end date
	res, reason := validate(c.DB, ctx.CampaignID, columnsToUpdate)
	if res != true {
		return ctx.BadRequest([]byte(reason))
	}
	trans, _ := c.DB.Begin()
	if !reflect.ValueOf(ctx.Payload.Messages).IsNil() {
		messagePercentage = getMessagePercentage(c.DB, ctx)
		for _, v := range messagePercentage {
			totalPercentage = totalPercentage + v
		}
	//validate if total percentage of new and existing messageId is 100
		if totalPercentage != 100 {
			return ctx.BadRequest([]byte("Total percentage of messages is not 100"))
		}
	// insert messageId and percentage if exists

		for message, percentage := range messagePercentage {
			_, err := trans.Exec("insert into campaign_message values(?,?,?) ON DUPLICATE key update "+
				"percentage=?", ctx.CampaignID, message, percentage, percentage)
			if err != nil {
				trans.Rollback()
				return ctx.InternalServerError([]byte("insertion to database failed"))
			}
		}
	}

	_,_,status := getParamsForValidation(c.DB,ctx.CampaignID)
	if !reflect.ValueOf(ctx.Payload.Status).IsNil()&&checkStatus(*ctx.Payload.Status,status) ==0 {
		return ctx.BadRequest([]byte("status cannot be changed"))
	}

	// update columns other than messageId and Percentage
	if len(columnsToUpdate) > 0 {
	for k, v := range columnsToUpdate {
		buffer.WriteString(k + `=` + formatter(v.Elem()) + `,`)
	}
	updateQuery := buffer.String()
	updateQuery = strings.TrimRight(updateQuery, ",")
	_, err := trans.Exec("update campaign SET "+updateQuery+" where campaignId=?", ctx.CampaignID)

	if err != nil {
		trans.Rollback()
		return ctx.InternalServerError([]byte("insertion to database failed"))
	}
}
	err = trans.Commit()
	if err != nil {
		return ctx.InternalServerError([]byte("insertion to database failed"))

	}
	stmt, _ = c.DB.Prepare("select campaignId,productId,status,startDate," +
		"endDate,activeStartHour,activeStartMinute,activeHoursInMinutes,pollingInterval,executionFrequency from campaign where campaignId =?")
	row, _ = stmt.Query(ctx.CampaignID)
	defer row.Close()
	if !row.Next() {
		return ctx.NotFound()
	}
	var campaign app.CampaignDetailed
	_ = row.Scan(&campaign.CampaignID,
		&campaign.ProductID,
		&campaign.Status,
		&campaign.StartDate,
		&campaign.EndDate,
		&campaign.ActiveStartHour,
		&campaign.ActiveStartMinute,
		&campaign.ActiveHoursInMinutes,
		&campaign.PollingInterval,
		&campaign.ExecutionFrequency)
		b,_ := json.Marshal(campaign)
		ctx.ResponseData.Write(b)


	var campSchedule app.Campaign

	err = c.DbMap.SelectOne(&campSchedule, "select * from campaign where campaignId=?",ctx.CampaignID) //STARTED OR APPROVED
	if err != nil {
		log.Println(err)
	}

	if *campSchedule.Status ==2 && ctx.Payload.Status != nil {
		scheduleCampaign(c.Scheduler,&campSchedule,c.Config,c.DbMap)
	} else if *campSchedule.Status ==32 && ctx.Payload.Status != nil{
		c.Scheduler.Remove(ctx.CampaignID)

	}else {
		c.Scheduler.Remove(ctx.CampaignID)
		scheduleCampaign(c.Scheduler,&campSchedule,c.Config,c.DbMap)
	}

	// CampaignsController_Update: end_implement
	return nil
}

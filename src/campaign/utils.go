package main

import (
	"database/sql"
	"fmt"
	"strings"
	"reflect"
	"campaign/app"
	"time"
	"strconv"
)

func getPaginationParams(ctx *app.GetAllCampaignsContext) (int, int) {
	var offset, numOfResults int
	if !reflect.ValueOf(ctx.NumOfResults).IsNil() {
		if !reflect.ValueOf(ctx.Offset).IsNil() {
			offset = int(*ctx.Offset)
		} else {
			offset = 0
		}
		numOfResults = int(*ctx.NumOfResults)
	} else {
		numOfResults = 10
	}
	return offset, numOfResults
}

func getCondition(ctx *app.GetAllCampaignsContext) string {
	var status int
	var condition string
	if !reflect.ValueOf(ctx.State).IsNil() {
		status = int(*ctx.State)
		condition = " where status=" + strconv.Itoa(status)
	} else {
		condition = ""
		status = 0
	}
	return condition
}
//
//func compareDatesForCreation(ctx *app.CreateCampaignsContext, smallDate int, largeDate int) error {
//	if smallDate > largeDate {
//		ctx.ResponseData.WriteHeader(400)
//		return ctx.BadRequest([]byte("date is not correct"))
//	}
//	return nil
//}

func campaignCreationValidation(ctx *app.CreateCampaignsContext) (bool,string) {
	var timeNow = int64(time.Now().Unix())
	if !compareDates(int64(ctx.Payload.StartDate), int64(ctx.Payload.EndDate))||
	!compareDates(timeNow, int64(ctx.Payload.StartDate))||
	!compareDates(timeNow, int64(ctx.Payload.EndDate)) {
		return false,"date is not correct"
	}
	var totalPercentage = 0.0
	for _, message := range ctx.Payload.Messages {
		totalPercentage = totalPercentage + message.Percentage
	}
	campaignDuration :=*ctx.Payload.ActiveStartHour*60+*ctx.Payload.ActiveStartMinute+*ctx.Payload.ActiveHoursInMinutes
	if campaignDuration/60 >24 {
		return false,"Active hours in minutes not correct"
	}
	if totalPercentage != 100 {

		return false,"Total percentage of messages is not 100" // Message
	}
	return true,""
}

func getTableColumns(db *sql.DB, tableSchema string, tableName string) map[string]string {
	stmt, _ := db.Prepare("SELECT COLUMN_NAME  FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_SCHEMA=? AND TABLE_NAME=?")
	row, _ := stmt.Query(tableSchema, tableName)
	var columns = make(map[string]string)
	var column string
	for row.Next() {
		row.Scan(&column)
		columns[strings.ToLower(column)] = column
	}
	return columns
}

func getColumnsToUpdate(ctx *app.UpdateCampaignsContext, columns map[string]string) map[string]reflect.Value {
	var columnsToUpdate = make(map[string]reflect.Value)

	fieldPtr := reflect.ValueOf(ctx.Payload)
	fields := fieldPtr.Elem()
	for i := 0; i < fields.NumField(); i++ {
		field := fields.Field(i)
		fieldName := strings.ToLower(fieldPtr.Elem().Type().Field(i).Name)
		if field.IsNil() != true && fieldName != "messages" {
			//val := field.Elem()
			//fmt.Println(val)
			columnsToUpdate[columns[fieldName]] = field
		}
	}
	return columnsToUpdate
}

func compareDates(smallDate int64, largeDate int64) bool {
	if smallDate > largeDate {
		return false
	}
	return true
}

func getParamsForValidation(db *sql.DB,id string) (int,int,int){
	var startDateOld, endDateOld, statusOld int
	stmt, _ := db.Prepare("SELECT startdate,enddate,status"+" FROM campaign  WHERE campaignId=?")
	row, _ := stmt.Query(id)
	row.Next()
	_= row.Scan(&startDateOld, &endDateOld, &statusOld)
	return startDateOld, endDateOld, statusOld
}


func validate(db *sql.DB, campaignId string, columnsToUpdate map[string]reflect.Value) (bool,string) {
	var timeNow = int64(time.Now().Unix())
	var startDateOld, endDateOld, statusOld = getParamsForValidation(db,campaignId)
	if statusNew, exists := columnsToUpdate["status"]; exists && statusNew.Elem().Int() < 8 || statusOld < 8 {
		if startDateNew, exists := columnsToUpdate["startdate"]; exists{
			if endDateNew, exists := columnsToUpdate["enddate"]; exists &&
				(!compareDates(startDateNew.Elem().Int(), endDateNew.Elem().Int())||
				!compareDates(timeNow, endDateNew.Elem().Int())||
				!compareDates(timeNow, startDateNew.Elem().Int())){
					return false,"date not correct"
			}
			if !compareDates(timeNow, startDateNew.Elem().Int())||
				!compareDates(startDateNew.Elem().Int(),int64(endDateOld)){
				return false,"start date not correct"
			}

		} else if endDateNew, exists := columnsToUpdate["enddate"]; exists &&
			(compareDates(int64(startDateOld), endDateNew.Elem().Int()) ||
			compareDates(timeNow, endDateNew.Elem().Int())){
			return false,"end date not correct"
		}
	} else if statusNew, exists := columnsToUpdate["status"]; exists && statusNew.Elem().Int() >= 8 || statusOld >= 8 {
		if _, exists := columnsToUpdate["startdate"]; exists {
			return false,"Cannot change start date"
		}
		if endDateNew, exists := columnsToUpdate["enddate"]; exists &&
			(!compareDates(int64(startDateOld), endDateNew.Elem().Int()) ||
			!compareDates(timeNow, endDateNew.Elem().Int()) ){
				return false,"end date not correct"
		}
	}
	return true,""
}

func checkStatus(newStatus,status int) int {
	fmt.Println(newStatus,status)

	if (newStatus == CAMPAIGN_STATUS_REJECTED || newStatus == CAMPAIGN_STATUS_APPROVED) &&status == CAMPAIGN_STATUS_CREATED {
		return newStatus
	} else if newStatus == CAMPAIGN_STATUS_STARTED && (status == CAMPAIGN_STATUS_APPROVED || status == CAMPAIGN_STATUS_EXECUTING || status == CAMPAIGN_STATUS_STOPPED) {
		return newStatus
	} else if newStatus == CAMPAIGN_STATUS_EXECUTING && status == CAMPAIGN_STATUS_STARTED {
		return newStatus
	}  else if newStatus == CAMPAIGN_STATUS_STOPPED &&( status== CAMPAIGN_STATUS_EXECUTING || status == CAMPAIGN_STATUS_STARTED ) {
		return newStatus
	} else if newStatus == CAMPAIGN_STATUS_TERMINATED &&( status == CAMPAIGN_STATUS_STARTED ||status == CAMPAIGN_STATUS_STOPPED || status == CAMPAIGN_STATUS_APPROVED || status == CAMPAIGN_STATUS_REJECTED ) {
		return newStatus
	}
	return 0
}

func getMessagePercentage(db *sql.DB, ctx *app.UpdateCampaignsContext) map[string]float64 {
	var messagePercentage = make(map[string]float64)
	var message string
	var percentage float64
	stmt1, _ := db.Prepare("SELECT messageId,percentage FROM  campaign_message WHERE campaignId =?")
	row, _ := stmt1.Query(ctx.CampaignID)
	for row.Next() {
		_ = row.Scan(&message, &percentage)
		messagePercentage[message] = percentage
	}

	for _, message := range ctx.Payload.Messages {
		messagePercentage[message.MessageID] = message.Percentage
	}
	return messagePercentage
}

func formatter(v reflect.Value) string {
	switch v.Kind() {
	case reflect.Int:
		return strconv.FormatInt(v.Int(), 10)
	case reflect.String:
		return strconv.Quote(v.String())
	}
	return v.Type().String()
}

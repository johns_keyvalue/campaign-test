//go:generate goagen bootstrap -d campaign/design

package main

import (
	"os"
	"io/ioutil"
	"log"
	"database/sql"
	"github.com/go-gorp/gorp"
	"encoding/json"
	_ "github.com/go-sql-driver/mysql"
	"github.com/DavidHuie/gomigrate"
	"github.com/goadesign/goa"
	"github.com/goadesign/goa/middleware"
	"campaign/app"
	"campaign/scheduler"
)

var DEFAULT_CONF_FILE string = "config.json"

type ConfigScheduler struct {
	PollingIntervalInMinutes int32 `json:"pollingInterval"`
	ExecutionFrequencyInDays int32 `json:"executionFrequencyInDays"`
	ActiveStartHours         int32 `json:"activeStartHours"`
	ActiveStartMinutes       int32 `json:"activeStartMinutes"`
	ActiveEndHours           int32 `json:"activeEndHours"`
	ActiveEndMinutes         int32 `json:"activeEndMinutes"`
}

type ConfigEndPoint struct {
	CustomerServiceURL            string `json:"customerServiceURL"`
	DialogoueServiceURL           string `json:"dialogoueServiceURL"`
	ProductServiceURL             string `json:"productServiceURL"`
	LeadManagementServiceURL      string `json:"leadManagementServiceURL"`
	SmsTrackerServiceURL          string `json:"smsTrackerServiceURL"`
	SmsTrackerCallbackRegisterURL string `json:"smsTrackerCallbackRegisterURL"`
}

type ConfigDb struct {
	MigrationDir     string `json:"migrationsDir"`
	PublicKeyPath    string `json:"publicKeyPath"`
	Port             string `json:"port"`
	DbDriverName     string `json:"dbDriverName"`
	DbDataSourceName string `json:"dbDataSourceName"`
	DbInfo           string `json:"dbInfo"`
}

type ConfigSelection struct {
	PoolAvailability int      `json:"poolAvailability"`
	TransactionSize  int      `json:"transactionSize"`
	SelectQueries    []string `json:"selectQueries"`
}

type Config struct {
	DbConfig        ConfigDb        `json:"dbConfig"`
	SchedulerConfig ConfigScheduler `json:"schedulerConfig"`
	EndPointConfig  ConfigEndPoint  `json:"endPointsConfig"`
	SelectionConfig ConfigSelection `json:"selectionConfig"`
	ServerConfig    ConfigServer    `json:"serverConfig"`
}

type ConfigServer struct {
	Address string `json:"address"`
}

func InitDb(config ConfigDb) (*sql.DB, *gorp.DbMap) {

	db, err := sql.Open(config.DbDriverName, config.DbDataSourceName)
	checkErr(err, "sql.Open failed. Could not connect to the campaign Database")
	// construct a gorp DbMap
	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.MySQLDialect{Engine: config.DbInfo, Encoding: "UTF8"}}
	return db, dbmap
}

func LoadConfig(configFile string) (*Config, error) {

	if nil == &configFile {
		configFile = DEFAULT_CONF_FILE
	}
	conf, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, err
	}
	var configs Config
	err = json.Unmarshal(conf, &configs)
	if nil != err {
		return nil, err
	}
	return &configs, nil
}

func startServices(config *Config, dbmap *gorp.DbMap, scheduler *scheduler.Scheduler, db *sql.DB) error {

	service := goa.New("campaign")
	// Mount middleware
	service.Use(middleware.RequestID())
	service.Use(middleware.LogRequest(true))
	service.Use(middleware.ErrorHandler(service, true))
	service.Use(middleware.Recover())
	// Mount "campaigns" controller
	c := NewCampaignsController(service, db, scheduler, config, dbmap)
	app.MountCampaignsController(service, c)
	// Mount "messagecontents" controller
	c2 := NewMessagecontentsController(service, db)
	app.MountMessagecontentsController(service, c2)
	// Mount "swagger" controller
	c3 := NewSwaggerController(service)
	app.MountSwaggerController(service, c3)
	c4 := NewProductsController(service)
	app.MountProductsController(service, c4)
	c5 := NewLeadController(service)
	app.MountLeadController(service, c5)
	if err := service.ListenAndServe(config.ServerConfig.Address); err != nil {
		service.LogError("startup", "err", err)
	}
	return nil

}

func migrate(db *sql.DB, config *Config) error {

	migrator, err := gomigrate.NewMigrator(db, gomigrate.Mysql{}, config.DbConfig.MigrationDir)
	if err != nil {
		checkErr(err, "Could not instantiate migrator")
		os.Exit(1)
	}
	return migrator.Migrate()
}

func validateConfig(config *Config) error {

	//TODO validation
	return nil
}

func initCampaign() {

	var configPath = "config.json"
	if len(os.Args) > 1 {
		configPath = os.Args[1]
	}
	config, err := LoadConfig(configPath)
	if config == nil {
		checkErr(err, "Unable to read configuration.")
		os.Exit(1)
	}

	err = validateConfig(config)

	if nil != err {
		checkErr(err, "Invalid configuration.")
		os.Exit(1)
	}

	db, dbmap := InitDb(config.DbConfig)
	dbmap.AddTableWithName(app.Campaign{}, "campaign").SetKeys(false, "CampaignID")
	dbmap.AddTableWithName(app.CampaignExecutionContext{}, "execution_context").SetKeys(false, "ExecutionID")
	dbmap.AddTableWithName(CampaignData{}, "campaign_data").SetKeys(false, "CampaignDataId")

	defer dbmap.Db.Close()

	log.Println("Migrating...")
	err = migrate(db, config)

	if nil != err {
		checkErr(err, "Unable to do migration.")
		os.Exit(1)
	}
	scheduler := scheduler.New()
	log.Println("Starting campaign processing...")
	err = StartCampaign(config, dbmap, scheduler)

	if nil != err {
		checkErr(err, "Unable to start Campaign processing.")
		os.Exit(1)
	}

	log.Println("Starting campaign services...")
	err = startServices(config, dbmap, scheduler, db)
	if nil != err {
		checkErr(err, "Unable to start Campaign services.")
		os.Exit(1)
	}

}

func main() {

	initCampaign()
}

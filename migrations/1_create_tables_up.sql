CREATE TABLE message_content(

  messageId varchar(36) ,
  content varchar(160) NOT NULL,
  PRIMARY KEY (messageId)
);


CREATE TABLE campaign(

  campaignId varchar(36) ,
  productId varchar(36),
  status BIGINT DEFAULT 0,
  startdate  BIGINT,
  enddate BIGINT,

  activeStartHour integer,
  activeStartMinute integer,
  activeHoursInMinutes integer,

  pollingInterval integer,
  executionFrequency integer,
  currentExecutionCycleStartTime BIGINT,
  lastExecutionTime BIGINT,
  currentTargetVolume integer,

  approvedOn BIGINT,
  approvedBy varchar(36) ,
  PRIMARY KEY (campaignId)
);


CREATE TABLE campaign_data (

  campaignDataId varchar(36),
  phoneNumber varchar(36) NOT NULL,
  campaignId varchar(36),
  messageId varchar(36),
  sentDate BIGINT ,
  replyDate BIGINT,
  forwardedDate BIGINT,
  fiId varchar(36),
  reply varchar(160),
  PRIMARY KEY (campaignDataId),
  CONSTRAINT message_content_constr FOREIGN KEY (messageId) REFERENCES message_content (messageId),
  CONSTRAINT campaign_constraint FOREIGN KEY (campaignId) REFERENCES campaign (campaignId)
);

CREATE TABLE campaign_message (

  campaignId varchar(36),
  messageId varchar(36),
  percentage DECIMAL,

  PRIMARY KEY (campaignId,messageId),
  CONSTRAINT campaign_message_camp_constraint FOREIGN KEY (campaignId) REFERENCES campaign (campaignId),
  CONSTRAINT campaign_message_content_constraint FOREIGN KEY (messageId) REFERENCES message_content (messageId)
);

CREATE TABLE `execution_context` (
  `executionId` varchar(36) NOT NULL,
  `campaignId` varchar(36) DEFAULT NULL,
  `startTime` bigint(20) DEFAULT NULL,
  `error` varchar(36) DEFAULT NULL ,
  `endTime` bigint(20) DEFAULT NULL,
  `numMessagesSent` int(11) DEFAULT NULL,
  PRIMARY KEY (`executionId`),
  KEY `camp_exe_constraint` (`campaignId`),
  CONSTRAINT `camp_exe_constraint` FOREIGN KEY (`campaignId`) REFERENCES `campaign` (`campaignId`)
);

CREATE TABLE `loan_pool` (
  `phone_number` varchar(36) DEFAULT NULL,
  `province` varchar(256) DEFAULT NULL,
  `district` varchar(256) DEFAULT NULL,
  `credit_score` decimal(10,0) DEFAULT NULL,
  `demand_score` decimal(10,0) DEFAULT NULL,
  `demand_prob` decimal(10,2) DEFAULT NULL,
  `income_level` decimal(10,2) DEFAULT NULL,
  `update_date` bigint(20) DEFAULT NULL,
  `available_date` bigint(20) DEFAULT NULL
);